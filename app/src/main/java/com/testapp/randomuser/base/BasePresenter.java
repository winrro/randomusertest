package com.testapp.randomuser.base;

public interface BasePresenter {
    void start();
}

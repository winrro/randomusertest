package com.testapp.randomuser.base;

public interface BaseView<T> {
    void setPresenter(T presenter);
}

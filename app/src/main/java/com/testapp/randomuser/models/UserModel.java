package com.testapp.randomuser.models;

import android.os.Parcel;
import android.os.Parcelable;

public class UserModel implements Parcelable {

    private String mFirstName;
    private String mLastName;
    private String mSmallPhoto;
    private String mBigPhoto;
    private String mAge;
    private String mDate;
    private String mGender;
    private String mStreet;
    private String mCity;
    private String mBirthday;
    private String mEmail;

    public UserModel(String mFirstName, String mLastName, String mSmallPhoto, String mBigPhoto,
                     String mAge, String mDate, String mGender, String mStreet, String mCity, String mBirthday, String mEmail) {
        this.mFirstName = mFirstName;
        this.mLastName = mLastName;
        this.mSmallPhoto = mSmallPhoto;
        this.mBigPhoto = mBigPhoto;
        this.mAge = mAge;
        this.mDate = mDate;
        this.mGender = mGender;
        this.mStreet = mStreet;
        this.mCity = mCity;
        this.mBirthday = mBirthday;
        this.mEmail = mEmail;
    }


    protected UserModel(Parcel in) {
        mFirstName = in.readString();
        mLastName = in.readString();
        mSmallPhoto = in.readString();
        mBigPhoto = in.readString();
        mAge = in.readString();
        mDate = in.readString();
        mGender = in.readString();
        mStreet = in.readString();
        mCity = in.readString();
        mBirthday = in.readString();
        mEmail = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mFirstName);
        dest.writeString(mLastName);
        dest.writeString(mSmallPhoto);
        dest.writeString(mBigPhoto);
        dest.writeString(mAge);
        dest.writeString(mDate);
        dest.writeString(mGender);
        dest.writeString(mStreet);
        dest.writeString(mCity);
        dest.writeString(mBirthday);
        dest.writeString(mEmail);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    public String getmFirstName() {
        return mFirstName;
    }

    public String getmLastName() {
        return mLastName;
    }

    public String getmSmallPhoto() {
        return mSmallPhoto;
    }

    public String getmBigPhoto() {
        return mBigPhoto;
    }

    public String getmAge() {
        return mAge;
    }

    public String getmDate() {
        return mDate;
    }


    public String getmGender() {
        return mGender;
    }

    public String getmStreet() {
        return mStreet;
    }

    public String getmCity() {
        return mCity;
    }

    public String getmBirthday() {
        return mBirthday;
    }

    public String getmEmail() {
        return mEmail;
    }
}

package com.testapp.randomuser.ui.fragments.list;

import com.testapp.randomuser.base.BasePresenter;
import com.testapp.randomuser.base.BaseView;
import com.testapp.randomuser.models.UserModel;

import java.util.List;

public interface ListContract {

    interface View extends BaseView<Presenter> {
        void addUsersToList(List<UserModel> newData);
    }

    interface Presenter extends BasePresenter {
        void loadUsers(int page, String seed);
    }
}

package com.testapp.randomuser.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.testapp.randomuser.R;
import com.testapp.randomuser.ui.fragments.list.ListFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_view, new ListFragment())
                .commit();

    }
}

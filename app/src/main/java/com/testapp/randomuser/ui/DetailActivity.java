package com.testapp.randomuser.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.testapp.randomuser.R;
import com.testapp.randomuser.models.UserModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DetailActivity extends AppCompatActivity {

    public static final String ARG_USER = "user_arg";
    private UserModel mUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mUser = getIntent().getParcelableExtra(ARG_USER);

        if (mUser == null) {
            onBackPressed();
        }

        ImageView photo = findViewById(R.id.photo);
        TextView name = findViewById(R.id.name);
        TextView date = findViewById(R.id.date);
        TextView gender = findViewById(R.id.gender);
        TextView location = findViewById(R.id.location);
        TextView email = findViewById(R.id.email);

        Picasso.get().load(mUser.getmBigPhoto()).into(photo);
        name.setText(getResources().getString(R.string.list_name, mUser.getmFirstName(), mUser.getmLastName()));
        date.setText(formatDate());
        gender.setText(mUser.getmGender());
        location.setText(getResources().getString(R.string.location_label, mUser.getmCity(), mUser.getmStreet()));
        email.setText(mUser.getmEmail());
    }

    private String formatDate() {
        if (mUser.getmBirthday() == null) return "null";
        String pattern = "yyyy-MM-dd'T'HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.getDefault());
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
        Date date = null;
        try {
            date = sdf.parse(mUser.getmBirthday());
            return df.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return mUser.getmBirthday();
        }

    }
}

package com.testapp.randomuser.ui.fragments.list;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.testapp.randomuser.EndlessRecyclerViewScrollListener;
import com.testapp.randomuser.R;
import com.testapp.randomuser.models.UserModel;
import com.testapp.randomuser.ui.DetailActivity;

import java.util.List;


public class ListFragment extends Fragment implements PeopleOnClickListener, ListContract.View {

    private PeopleListAdapter mAdapter = new PeopleListAdapter(this);
    private String mSeed = "";
    private EditText seedEd;
    private TextView seedLabel;

    private ListContract.Presenter mPresenter;

    public ListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = view.findViewById(R.id.rv_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
        DividerItemDecoration divider = new DividerItemDecoration(recyclerView.getContext(),
                DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(divider);

        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                mPresenter.loadUsers(page, mSeed);
            }
        };

        recyclerView.addOnScrollListener(scrollListener);

        seedEd = view.findViewById(R.id.ed_seed);
        seedLabel = view.findViewById(R.id.seed_label);

        seedEd.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    hideKeyboard(seedEd);
                    changeSeed();
                    return true;
                }
                return false;
            }

        });


        view.findViewById(R.id.btn_apply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeSeed();
            }
        });

        new ListPresenter(this).start();
    }

    private void changeSeed() {
        mSeed = seedEd.getText().toString();
        mAdapter.clear();
        seedLabel.setText(getString(R.string.seed, mSeed));
        mPresenter.loadUsers(1, mSeed);
    }

    private void hideKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onListItemClicked(UserModel model) {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(DetailActivity.ARG_USER, model);
        startActivity(intent);
    }

    @Override
    public void addUsersToList(List<UserModel> newData) {
        mAdapter.addData(newData);
    }

    @Override
    public void setPresenter(ListContract.Presenter presenter) {
        mPresenter = presenter;
        mPresenter.loadUsers(1, mSeed);
    }
}

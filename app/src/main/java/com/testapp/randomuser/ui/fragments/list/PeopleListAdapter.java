package com.testapp.randomuser.ui.fragments.list;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.testapp.randomuser.R;
import com.testapp.randomuser.models.UserModel;

import java.util.ArrayList;
import java.util.List;

public class PeopleListAdapter extends RecyclerView.Adapter<PeopleListAdapter.MyViewHolder> {
    private List<UserModel> mDataList = new ArrayList<>();
    private PeopleOnClickListener mListener;

    PeopleListAdapter(PeopleOnClickListener listener) {
        mListener = listener;
    }

    void addData(List<UserModel> newData) {
        int startPos = mDataList.size();
        mDataList.addAll(newData);
        notifyItemRangeInserted(startPos, newData.size());
    }

    @NonNull
    @Override
    public PeopleListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new PeopleListAdapter.MyViewHolder(v);
    }

    void clear() {
        mDataList.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull PeopleListAdapter.MyViewHolder holder, int position) {
        holder.mName.setText(holder.mName.getResources().getString(
                R.string.list_name,
                getItem(position).getmFirstName(),
                getItem(position).getmLastName()));
        holder.mCity.setText(getItem(position).getmCity());
        holder.mAge.setText(getItem(position).getmAge());
        Picasso.get().load(getItem(position).getmSmallPhoto()).into(holder.mPhoto);

    }

    private UserModel getItem(int pos) {
        return mDataList.get(pos);
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mName;
        TextView mCity;
        TextView mAge;
        ImageView mPhoto;
        ConstraintLayout mRootView;

        MyViewHolder(View v) {
            super(v);
            mName = v.findViewById(R.id.name);
            mCity = v.findViewById(R.id.city);
            mAge = v.findViewById(R.id.age);
            mPhoto = v.findViewById(R.id.photo);
            mRootView = v.findViewById(R.id.root_view);
            mRootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onListItemClicked(getItem(getAdapterPosition()));
                }
            });
        }
    }
}
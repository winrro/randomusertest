package com.testapp.randomuser.ui.fragments.list;

import com.testapp.randomuser.models.ApiResultModel;
import com.testapp.randomuser.models.UserModel;
import com.testapp.randomuser.network.NetworkModule;
import com.testapp.randomuser.network.RandomUserApi;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListPresenter implements ListContract.Presenter {

    private final RandomUserApi mApi;
    private ListContract.View mView;

    public ListPresenter(ListContract.View view) {
        mApi = new NetworkModule().getRandomUserApi();
        mView = view;
    }

    @Override
    public void loadUsers(int page, String seed) {
        mApi.requestUsers(page, 10, seed).enqueue(new Callback<ApiResultModel>() {
            @Override
            public void onResponse(Call<ApiResultModel> call, Response<ApiResultModel> response) {
                ArrayList<UserModel> userList = new ArrayList<>();
                if (response.body() != null) {
                    for (ApiResultModel.Result res : response.body().results) {
                        userList.add(new UserModel(
                                res.name.first,
                                res.name.last,
                                res.picture.medium,
                                res.picture.large,
                                res.dob.age.toString(),
                                res.registered.date,
                                res.gender,
                                res.location.street,
                                res.location.city,
                                res.dob.date,
                                res.email));
                    }
                }
                mView.addUsersToList(userList);
            }

            @Override
            public void onFailure(Call<ApiResultModel> call, Throwable t) {

            }
        });
    }

    @Override
    public void start() {
        mView.setPresenter(this);
    }
}

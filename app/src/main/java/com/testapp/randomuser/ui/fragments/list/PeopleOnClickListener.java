package com.testapp.randomuser.ui.fragments.list;

import com.testapp.randomuser.models.UserModel;

public interface PeopleOnClickListener {
    void onListItemClicked(UserModel model);
}

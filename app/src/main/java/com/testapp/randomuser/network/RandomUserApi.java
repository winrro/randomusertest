package com.testapp.randomuser.network;

import com.testapp.randomuser.models.ApiResultModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RandomUserApi {

    @GET("api/")
    Call<ApiResultModel> requestUsers(
            @Query("page") int page,
            @Query("results") int results,
            @Query("seed") String seed);

}

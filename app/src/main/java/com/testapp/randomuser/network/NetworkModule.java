package com.testapp.randomuser.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkModule {

    private static final String BASE_URL = "https://randomuser.me/";

    private RandomUserApi randomUserApi;

    public NetworkModule() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        randomUserApi = retrofit.create(RandomUserApi.class);
    }

    public RandomUserApi getRandomUserApi() {
        return randomUserApi;
    }
}
